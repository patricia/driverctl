Source: driverctl
Maintainer: Luca Boccassi <bluca@debian.org>
Priority: optional
Section: admin
Build-Depends: debhelper-compat (= 13), pkg-config, systemd, udev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://gitlab.com/driverctl/driverctl
Vcs-Git: https://salsa.debian.org/debian/driverctl.git
Vcs-Browser: https://salsa.debian.org/debian/driverctl

Package: driverctl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, udev,
Description: Device driver control utility for Linux
 Driverctl is a tool for manipulating and inspecting the system
 device driver choices.
 .
 Devices are normally assigned to their sole designated kernel driver
 by default. However in some situations it may be desirable to
 override that default, for example to try an older driver to
 work around a regression in a driver or to try an experimental alternative
 driver. Another common use-case is pass-through drivers and driver
 stubs to allow userspace to drive the device, such as in case of
 virtualization.
 .
 driverctl integrates with udev to support overriding
 driver selection for both cold- and hotplugged devices from the
 moment of discovery, but can also change already assigned drivers,
 assuming they are not in use by the system. The driver overrides
 created by driverctl are persistent across system reboots
 by default.
